$(".fc-day[data-date=2019-04-14]").css("backgroundColor","green");




function addClassByDate(date) {
    var dataAttr = getDataAttr(date);
    $("[data-date='" + dataAttr + "']").addClass("fc-day-falta");
}

function getDataAttr(date) {
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate().toString().length === 2 ? date.getDate() : "0" + date.getDate());
};