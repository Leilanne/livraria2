PrimeFaces.locales['pt'] = {
        closeText: 'Fechar',
        prevText: 'Anterior',
        nextText: 'Próximo',
        currentText: 'Data Atual',
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Noembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Maio','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Sábado','Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira'],
        dayNamesShort: ['Sab','Dom','Seg','Ter','Qua','Qui','Sex'],
        dayNamesMin: ['Sab','Dom','Seg','Ter','Qua','Qui','Sex'],
        weekHeader: 'Sem',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        month: 'Mês',
        week: 'Semana',
        day: 'Dia',
        allDayText : 'Todo dia'
    };


