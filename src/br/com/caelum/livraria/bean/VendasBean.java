package br.com.caelum.livraria.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

import br.com.caelum.livraria.dao.DAO;
import br.com.caelum.livraria.modelo.Livro;
import br.com.caelum.livraria.modelo.Venda;

@ManagedBean
@ViewScoped
public class VendasBean {

	public LineChartModel  getVendasModel() {

		LineChartModel  model = new LineChartModel ();
	    model.setAnimate(true);
	    
	    model.setTitle("Vendas");
	    model.setLegendPosition("ne");
	    
	    Axis xAxis = new CategoryAxis("Livros");
        model.getAxes().put(AxisType.X, xAxis);
        xAxis.setMax(5);
        Axis yAxis = model.getAxis(AxisType.Y);
        yAxis.setLabel("Quantidade");
        yAxis.setMin(0);
        yAxis.setMax(10);
	    ChartSeries vendaSerie = new ChartSeries();
	    vendaSerie.setLabel("Vendas 2016");

	    List<Venda> vendas = getVendas(1234);

	    for (Venda venda : vendas) {
	        vendaSerie.set(venda.getLivro().getTitulo(), venda.getQuantidade());
	    }

	    model.addSeries(vendaSerie);

	    ChartSeries vendaSerie2015 = new ChartSeries();
	    vendaSerie2015.setLabel("Vendas 2015");

	    vendas = getVendas(4321);

	    for (Venda venda : vendas) {
	        vendaSerie2015.set(venda.getLivro().getTitulo(),
	                venda.getQuantidade());
	    }

	    model.addSeries(vendaSerie2015);

	    return model;
	}

	public List<Venda> getVendas(long seed) {

	    List<Livro> livros = new DAO<Livro>(Livro.class).listaTodos();
	    List<Venda> vendas = new ArrayList<Venda>();

	    Random random = new Random(seed);

	    for (Livro livro : livros) {
	        Integer quantidade = random.nextInt(10);
	        vendas.add(new Venda(livro, quantidade));
	    }

	    return vendas;
	}
}
